<?php
require_once 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

use Timber\Timber;

use Theme\Admin\DeveloperOptions\DeveloperOptions;
use Theme\Admin\ThemeOptions\ThemeOptions;
use Theme\Admin\AdminBar;

use Theme\Config\ThemeSetup;
use Theme\Config\TimberConfig;
use Theme\Config\Assets;
use Theme\Config\CustomPostTypes;
use Theme\Config\CustomTaxonomies;
use Theme\Config\Menus;
use Theme\Config\Widget;
use Theme\Config\ThemeSupport;
use Theme\Config\SiteOriginConfig;

use Theme\Core\Module;

new Timber();
new DeveloperOptions();
new ThemeOptions();
new TimberConfig();

ThemeSetup::register();
AdminBar::register();
Assets::load();
CustomPostTypes::register();
CustomTaxonomies::register();
Menus::register();
Widget::register();
ThemeSupport::register();
Module::register();
SiteOriginConfig::register();
