<?php
/**
 * The template for displaying 404 pages (not found)
 */
 
$context = Timber::get_context();

$post = new Timber\Post();

$context['post'] = $post;

Timber::render(["404.twig", "page.twig"], $context);