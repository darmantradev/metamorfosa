<?php
/**
 * To render module on frontend, use shortcode [md-banner]
 */
namespace Theme\Modules\Banner;

use Theme\Core\Module as ThemeModule;

class Module extends ThemeModule
{
    public function processModule()
    {
        $config = array(
            'global_css' => true,
        );

        $this->setConfig($config);
    }
}