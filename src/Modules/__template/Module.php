<?php
/**
 * To render module on frontend, use shortcode [md-__ShortcodeName__]
 */
namespace Theme\Modules\__ModuleName__;

use Theme\Core\Module as ThemeModule;

class Module extends ThemeModule
{
    public function processModule()
    {
        //Set module configuration
        //Remove this section if not used
        $config = array(
            'global_css' => true, // enqueue CSS globally in header. By default, CSS will be enqueued in footer
            'attr' => array( // Shortcode attr
                'value' => 'Hello World'
            ),
            'context' => array( // Pass variable to template
                'test' => 'This is context variable'
            )
        );

        $this->setConfig($config);
        //End Module configuration
    }

    /**
     * Change template name based on shortcode attr
     * This function can be removed if not used
     */
    public function getTemplateName($attr)
    {
        if( $attr['value'] == 'Hello World') {
            return 'hello-world'; //hello-world.twig
        }else {
            return 'template'; //template.twig
        }
    }
}