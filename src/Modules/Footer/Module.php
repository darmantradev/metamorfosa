<?php
/**
 * To render module on frontend, use shortcode [md-footer]
 */
namespace Theme\Modules\Footer;

use Theme\Core\Module as ThemeModule;

class Module extends ThemeModule
{
    public function processModule()
    {
        $config = array(
            'global_css' => true,
        );

        $this->setConfig($config);
    }
}