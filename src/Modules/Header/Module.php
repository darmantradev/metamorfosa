<?php
namespace Theme\Modules\Header;

use Theme\Core\Module as ThemeModule;

class Module extends ThemeModule
{
    public function processModule()
    {
        $config = array(
            'global_css' => true,
        );

        $this->setConfig($config);
    }
}