const header = {
    init: () => {
        header.addClassOnScroll();
        header.mobileNav();
    },
    addClassOnScroll: () => {
        let topOffset = 0;
        window.addEventListener('scroll', () => {
            let scrollPos =
                window.scrollY ||
                window.scrollTop ||
                document.getElementsByTagName('html')[0].scrollTop;
        
            if (scrollPos > topOffset) {
                document.body.classList.add('scroll');
            } else {
                document.body.classList.remove('scroll');
            }
        });
    },
    mobileNav: () => {
        const hamburger = document.querySelector('.header__mobile--hamburger > .hamburger');

        if( !document.body.contains(hamburger) ) {
            return false;
        }

        hamburger.addEventListener('click', e => {
            e.preventDefault();
            document.body.classList.toggle('nav-active');
        });
    }
}

header.init();