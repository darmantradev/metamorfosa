<?php
/*
Widget Name: Experience Grid
*/

use Theme\Core\Widget as WidgetCore;

class Custom_Widget_ExperienceGrid extends WidgetCore 
{
  protected $config;

  public function __construct() 
  {
    $config = array(
      'name' => 'Experience Grid',
      'slug' => 'experience-grid',
      'dir'  => 'experience-grid',
      'file' => __FILE__  
    );

    parent::__construct($config);
	}

  /**
   * Find all available fields here: https://siteorigin.com/docs/widgets-bundle/form-building/form-fields/
   */
  public function get_widget_form() 
  {
		return array(
      'experiences' => array(
        'type'       => 'repeater',
        'label'      => 'Experience Grid',
        'item_name'  => 'Experience item',
        'item_label' => array(
          'selector'     => "[id*='title']",
          'update_event' => 'change',
          'value_method' => 'val'
        ),
        'fields'   => array(
          'title'  => array(
            'type'    => 'text',
            'label'   => 'Name' 
          ),
          'label' => array(
            'type'  => 'text',
            'label' => 'Label'
          ),
          'description' => array(
            'type'  => 'textarea',
            'label' => 'Description'
          ),
          'image' => array(
            'type'     => 'media',
            'label'    => 'Image',
            'fallback' => false 
          ),
          'button_label' => array(
            'type'  => 'text',
            'label' => 'Button Label'
          ),
        ) 
      )
	  );
  }
}

siteorigin_widget_register( 'experience-grid', __FILE__, 'Custom_Widget_ExperienceGrid' );
