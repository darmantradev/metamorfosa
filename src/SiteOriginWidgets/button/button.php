<?php
/*
Widget Name: Button
*/

use Theme\Core\Widget as WidgetCore;

class Custom_Widget_Button extends WidgetCore 
{
  protected $config;

  public function __construct() 
  {
    $config = array(
      'name' => 'Button',
      'slug' => 'button',
      'dir'  => 'button',
      'file' => __FILE__  
    );

    parent::__construct($config);
	}

  /**
   * Find all available fields here: https://siteorigin.com/docs/widgets-bundle/form-building/form-fields/
   */
  public function get_widget_form() 
  {
		return array(
      'label' => array(
        'type' => 'text',
        'label' => 'Button Label',
      ),
      'link' => array(
        'type'  => 'link',
        'label' => 'Button Link'
      ),
      'icon' => array(
        'type'  => 'text',
        'label' => 'Icon'
      ),
      'open_in_new_tab' => array(
        'type'     => 'checkbox',
        'label'    => 'Open in new tab',
      ),
	  );
  }
}

siteorigin_widget_register( 'button', __FILE__, 'Custom_Widget_Button' );
