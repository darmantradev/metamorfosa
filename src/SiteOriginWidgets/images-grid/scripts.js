const imagesGrid = {
    init: () => {
        if (typeof GLightbox === 'function') {
            const lightbox = GLightbox({
                selector: '.images-grid__link',
                touchNavigation: true,
                loop: true,
            });
        }
    }
}

imagesGrid.init();