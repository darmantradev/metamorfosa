<?php
/*
Widget Name: Images Grid
*/

use Theme\Core\Widget as WidgetCore;

class Custom_Widget_ImagesGrid extends WidgetCore 
{
  protected $config;

  public function __construct() 
  {
    $config = array(
      'name' => 'Images Grid',
      'slug' => 'images-grid',
      'dir'  => 'images-grid',
      'file' => __FILE__  
    );

    parent::__construct($config);
	}

  /**
   * Find all available fields here: https://siteorigin.com/docs/widgets-bundle/form-building/form-fields/
   */
  public function get_widget_form() 
  {
		return array(
      'images' => array(
        'type' => 'gallery',
        'label' => 'Images',
      ),
	  );
  }
}

siteorigin_widget_register( 'images-grid', __FILE__, 'Custom_Widget_ImagesGrid' );
