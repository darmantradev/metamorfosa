<?php
/*
Widget Name: Patreon Widget
*/

use Theme\Core\Widget as WidgetCore;

class Custom_Widget_PatreonWidget extends WidgetCore 
{
  protected $config;

  public function __construct() 
  {
    $config = array(
      'name' => 'Patreon Widget',
      'slug' => 'patreon-widget',
      'dir'  => 'patreon-widget',
      'file' => __FILE__  
    );

    parent::__construct($config);
	}

  /**
   * Find all available fields here: https://siteorigin.com/docs/widgets-bundle/form-building/form-fields/
   */
  public function get_widget_form() 
  {
		return array(
      'image' => array(
        'type'      => 'media',
        'label'     => 'Image',
        'library'   => 'image',
        'fallback'  => false
      ),
      'content' => array(
        'type'   => 'widget',
        'label'  => 'Content',
        'class'  => 'Custom_Widget_TextHeading',
        'hide'   => true 
      ),
      'button'  => array(
        'type'  => 'widget',
        'label' => 'Button',
        'class' => 'Custom_Widget_Button',
        'hide'  => true
      )
	  );
  }
}

siteorigin_widget_register( 'patreon-widget', __FILE__, 'Custom_Widget_PatreonWidget' );
