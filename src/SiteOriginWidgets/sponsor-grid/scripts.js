const sponsorGrid = {
    init: () => {
        sponsorGrid.initLIghtBox();
        sponsorGrid.numberCounter();
    },
    initLIghtBox: () => {
        if (typeof GLightbox === 'function') {
            const selectors = document.querySelectorAll('.trigger-modal');

            if( !document.body.contains(selectors[0]) ) {
                return false;
            }

            let index = 0;
            selectors.forEach(value => {
                const lightbox = GLightbox({
                    touchNavigation: false,
                    selector: '.glightbox-' + index,
                    draggable: false,
                    zoomable: false
                });
                
                lightbox.on('slide_after_load', (data) => {
                    const wrapper = data.trigger.closest('.sponsor-grid__footer'),
                          symbol = data.slide.querySelector('.give-currency-symbol.give-currency-position-before').innerText;

                    let number =  wrapper.querySelector('input[name="number"]').value,
                        price = wrapper.querySelector('input[name="price"]').value,
                        title = wrapper.querySelector('input[name="title"]').value,
                        amount = number * price;

                    data.slide.querySelector('#give-amount').setAttribute('readonly', 'readonly');
                    data.slide.querySelector('#give-amount').value = amount;
                    data.slide.querySelector('input[name="give-form-title"]').value = title;
                    data.slide.querySelector('.give-form-title').innerText = title;
                    data.slide.querySelector('.give-final-total-amount').getAttribute('data-total', amount);
                    data.slide.querySelector('.give-final-total-amount').innerText = symbol + amount;
                });

                index++;
            })
        }
    },
    numberCounter: () => {
        const numberCounter = document.querySelectorAll('.number-counter');

        if ( !document.body.contains(numberCounter[0]) ) {
            return false;
        }

        numberCounter.forEach(counter => {
            const arrows = counter.querySelectorAll('span.arrow'),
                  numberInput = counter.querySelector('input[type="number"'),
                  itemPrice = counter.querySelector('input[name="price"]'),
                  priceEL = counter.closest('.sponsor-grid__footer').querySelector('.total-price');

            let number = numberInput.value;

            arrows.forEach(arrow => {
                arrow.addEventListener('click', (e) => {
                    switch (e.target.getAttribute('data-counter')) {
                        case 'plus':
                                ++number;
                            break;
                        case 'minus':
                                if( number > 1 ) {
                                    --number;
                                }
                            break;
                    }

                    numberInput.value = number;
                    sponsorGrid.updatePrice( number, itemPrice.value, priceEL );
                });
            })
        });
    },

    updatePrice: (number, price, priceEL) => {
        let totalPrice =  number * price;
        priceEL.innerText = totalPrice;
    }
}

sponsorGrid.init();