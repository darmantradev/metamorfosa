<?php
/*
Widget Name: Sponsor Grid
*/

use Theme\Core\Widget as WidgetCore;

class Custom_Widget_SponsorGrid extends WidgetCore 
{
  protected $config;

  public function __construct() 
  {
    $config = array(
      'name' => 'Sponsor Grid',
      'slug' => 'sponsor-grid',
      'dir'  => 'sponsor-grid',
      'file' => __FILE__  
    );

    parent::__construct($config);
	}

  /**
   * Find all available fields here: https://siteorigin.com/docs/widgets-bundle/form-building/form-fields/
   */
  public function get_widget_form() 
  {
		return array(
      'sponsors' => array(
        'type'       => 'repeater',
        'label'      => 'Sponsors Grid',
        'item_name'  => 'Sponsor item',
        'item_label' => array(
          'selector'     => "[id*='title']",
          'update_event' => 'change',
          'value_method' => 'val'
        ),
        'fields'   => array(
          'title'  => array(
            'type'    => 'text',
            'label'   => 'Name' 
          ),
          'label' => array(
            'type'  => 'text',
            'label' => 'Label'
          ),
          'description' => array(
            'type'  => 'textarea',
            'label' => 'Description'
          ),
          'price' => array(
            'type'  => 'number',
            'label' => 'Price'
          ),
          'image' => array(
            'type'     => 'media',
            'label'    => 'Image',
            'fallback' => false 
          )
        ) 
      )
	  );
  }
}

siteorigin_widget_register( 'sponsor-grid', __FILE__, 'Custom_Widget_SponsorGrid' );
