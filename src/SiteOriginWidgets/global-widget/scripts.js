(function($) {
    
    $( document ).on( 'sowsetupformfield', '.siteorigin-widget-field-type-select', (e) => {
        const base = e.target.closest('.siteorigin-widget-form');
        if( base.getAttribute('data-id-base') == 'global-widget' ) {
            e.target.querySelector('select').addEventListener('change', event => {
                
                const text = event.target.options[event.target.selectedIndex].text,
                      input = base.querySelector('.siteorigin-widget-field-type-text input');

                let title = 'No widget selected';

                if( event.target.value != 0 ) {
                    title = text;
                }

                input.value = title;
            });
        }
    });

})( jQuery );