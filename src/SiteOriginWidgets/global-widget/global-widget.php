<?php
/*
Widget Name: Global Widget
*/

use Theme\Core\Widget as WidgetCore;

class Custom_Widget_GlobalWidget extends WidgetCore 
{
  protected $config;

  public function __construct() 
  {
    $config = array(
      'name' => 'Global Widget',
      'slug' => 'global-widget',
      'dir'  => 'global-widget',
      'file' => __FILE__  
    );

    parent::__construct($config);
	}

  /**
   * Find all available fields here: https://siteorigin.com/docs/widgets-bundle/form-building/form-fields/
   */
  public function get_widget_form() 
  {
		return array(
      'text' => array(
        'type' => 'text',
        'label' => 'Title',
      ),
      'widget_selector' => array(
        'type' => 'select',
        'label' => 'Select global widget',
        'options' => $this->get_global_widgets()
      )
	  );
  }

  public function get_global_widgets()
  {
    $global_widget = array(0 => 'Select global widget');

    $args = array(
      'post_type'       => 'global-widget',
      'post_status'     => 'publish',
      'posts_per_page'  => -1
    );

    $query = new WP_Query( $args );

    if( $query->have_posts() ) {
      while( $query->have_posts() ) {
        $query->the_post();
        $global_widget[get_the_ID()] = get_the_title();
      }
    }

    wp_reset_postdata();

    return $global_widget;
  }
}

siteorigin_widget_register( 'global-widget', __FILE__, 'Custom_Widget_GlobalWidget' );
