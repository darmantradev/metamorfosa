<?php
  $global_widget = $instance['widget_selector'];

  if( $global_widget == 0 ) {
    return;
  }

  if ( class_exists( 'SiteOrigin_Panels' ) && get_post_meta( $global_widget, 'panels_data', true ) ) {
    echo SiteOrigin_Panels::renderer()->render( $global_widget );
  } else {
    $content = wpautop(get_post( $global_widget )->post_content);
    echo $content;
  }