<?php
/*
Widget Name: Content Grid
*/

use Theme\Core\Widget as WidgetCore;

class Custom_Widget_ContentGrid extends WidgetCore 
{
  protected $config;

  public function __construct() 
  {
    $config = array(
      'name' => 'Content Grid',
      'slug' => 'content-grid',
      'dir'  => 'content-grid',
      'file' => __FILE__  
    );

    parent::__construct($config);
	}

  /**
   * Find all available fields here: https://siteorigin.com/docs/widgets-bundle/form-building/form-fields/
   */
  public function get_widget_form() 
  {
		return array(
      'style' => array(
        'type' => 'select',
        'label' => 'Style',
        'options' => array(
          'default' => 'default',
          'partner' => 'partner'
        )
      ),
      'column' => array(
        'type'     => 'select',
        'label'    => 'Choose columns',
        'options'  => array(
          '3'     => '3 Columns',
          '4'     => '4 Columns'
        ),
        'default'  => '3'
      ),
      'text' => array(
          'type'  => 'text',
          'label' => 'Title'
      ),
      'grids' => array(
        'type'        => 'repeater',
        'label'       => 'Grid repeater',
        'item_name'   => 'Grid item',
        'item_label' => array(
          'selector'     => "[id*='title']",
          'update_event' => 'change',
          'value_method' => 'val'
        ),
        'fields' => array(
          'title' => array(
            'type'  => 'text',
            'label' => 'Title'
          ),
          'subtitle' => array(
            'type'  => 'tinymce',
            'label' => 'Subtitle'
          ),
          'description' => array(
            'type'  => 'textarea',
            'label' => 'Description'
          ),
          'image' => array(
            'type'     => 'media',
            'label'    => 'Image',
            'fallback' => false
          ),
          'button' => array(
            'type'   => 'widget',
            'label'  => 'Button',
            'class'  => 'Custom_Widget_Button',
            'hide'   => true 
          )
        )
      )
	  );
  }
}

siteorigin_widget_register( 'content-grid', __FILE__, 'Custom_Widget_ContentGrid' );
