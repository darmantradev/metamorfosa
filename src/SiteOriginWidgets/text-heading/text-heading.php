<?php
/*
Widget Name: Text Heading
*/

use Theme\Core\Widget as WidgetCore;

class Custom_Widget_TextHeading extends WidgetCore {
	function __construct() {
    
    $config = array(
      'name' => 'Text Heading',
      'slug' => 'text-heading',
      'dir'  => 'text-heading',
      'file' => __FILE__  
    );

    parent::__construct($config);
	}

  /**
   * Find all available fields here: https://siteorigin.com/docs/widgets-bundle/form-building/form-fields/
   */
	function get_widget_form() {
		return array(
      'text' => array(
        'type' => 'text',
        'label' => 'Title'
      ),
      'description' => array(
        'type' => 'textarea',
        'label' => 'Description'
      ),
	  );
  }
}

siteorigin_widget_register( 'text-heading', __FILE__, 'Custom_Widget_TextHeading' );
