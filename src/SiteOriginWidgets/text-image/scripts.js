if (typeof GLightbox === 'function') {
    const lightbox = GLightbox({
        touchNavigation: true,
        autoplayVideos: true
    });   
}