<?php
/*
Widget Name: Text Image
*/

use Theme\Core\Widget as WidgetCore;

class Custom_Widget_TextImage extends WidgetCore 
{
  protected $config;

  public function __construct() 
  {
    $config = array(
      'name' => 'Text Image',
      'slug' => 'text-image',
      'dir'  => 'text-image',
      'file' => __FILE__  
    );

    parent::__construct($config);
	}

  /**
   * Find all available fields here: https://siteorigin.com/docs/widgets-bundle/form-building/form-fields/
   */
  public function get_widget_form() 
  {
		return array(
      'image' => array(
        'type'      => 'media',
        'label'     => 'Video Thumbnail',
        'library'   => 'image',
        'fallback'  => false
      ),
      'video_link' => array(
        'type'  => 'link',
        'label' => 'Video URL'
      ),
      'content' => array(
        'type'   => 'widget',
        'label'  => 'Content',
        'class'  => 'Custom_Widget_TextHeading',
        'hide'   => true 
      ),
      'button' => array(
        'type'   => 'widget',
        'label'  => 'Button',
        'class'  => 'Custom_Widget_Button',
        'hide'   => true 
      ),
      'image_right' => array(
        'type'     => 'checkbox',
        'label'    => 'Image on the right side',
      ),
	  );
  }
}

siteorigin_widget_register( 'text-image', __FILE__, 'Custom_Widget_TextImage' );
