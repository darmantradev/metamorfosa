<?php
/*
Widget Name: Our Team
*/

use Theme\Core\Widget as WidgetCore;

class Custom_Widget_OurTeam extends WidgetCore 
{
  protected $config;

  public function __construct() 
  {
    $config = array(
      'name' => 'Our Team',
      'slug' => 'our-team',
      'dir'  => 'our-team',
      'file' => __FILE__  
    );

    parent::__construct($config);
	}

  /**
   * Find all available fields here: https://siteorigin.com/docs/widgets-bundle/form-building/form-fields/
   */
  public function get_widget_form() 
  {
		return array(
      'column' => array(
        'type'     => 'select',
        'label'    => 'Column',
        'options'  => array(
          '6' => '6 Columns',
          '4' => '4 Columns'
        ) 
      ),
      'our_team' => array(
        'type'        => 'repeater',
        'label'       => 'Team repeater',
        'item_name'   => 'Team item',
        'item_label' => array(
          'selector'     => "[id*='name']",
          'update_event' => 'change',
          'value_method' => 'val'
        ),
        'fields' => array(
          'name' => array(
            'type'  => 'text',
            'label' => 'Name'
          ),
          'position' => array(
            'type'  => 'text',
            'label' => 'Position'
          ),
          'image' => array(
            'type'     => 'media',
            'label'    => 'Image',
            'fallback' => false
          )
        )
      )
	  );
  }
}

siteorigin_widget_register( 'our-team', __FILE__, 'Custom_Widget_OurTeam' );
