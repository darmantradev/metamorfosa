<?php
/*
Widget Name: Call To Action
*/

use Theme\Core\Widget as WidgetCore;

class Custom_Widget_CallToAction extends WidgetCore 
{
  protected $config;

  public function __construct() 
  {
    $config = array(
      'name' => 'Call To Action',
      'slug' => 'call-to-action',
      'dir'  => 'call-to-action',
      'file' => __FILE__  
    );

    parent::__construct($config);
	}

  /**
   * Find all available fields here: https://siteorigin.com/docs/widgets-bundle/form-building/form-fields/
   */
  public function get_widget_form() 
  {
		return array(
      'text' => array(
        'type' => 'text',
        'label' => 'Title',
      ),
      'description' => array(
          'type'  => 'textarea',
          'label' => 'Description'
      ),
      'image' => array(
        'type'     => 'media',
        'label'    => 'Background',
        'fallback' => false 
      ),
      'button' => array(
        'type'   => 'widget',
        'label'  => 'Button',
        'class'  => 'Custom_Widget_Button',
        'hide'   => true 
      )
	  );
  }
}

siteorigin_widget_register( 'call-to-action', __FILE__, 'Custom_Widget_CallToAction' );
