<?php
/*
Widget Name: Shortcode Widget
*/

use Theme\Core\Widget as WidgetCore;

class Custom_Widget_ShortcodeWidget extends WidgetCore 
{
  protected $config;

  public function __construct() 
  {
    $config = array(
      'name' => 'Shortcode Widget',
      'slug' => 'shortcode-widget',
      'dir'  => 'shortcode-widget',
      'file' => __FILE__  
    );

    parent::__construct($config);
	}

  /**
   * Find all available fields here: https://siteorigin.com/docs/widgets-bundle/form-building/form-fields/
   */
  public function get_widget_form() 
  {
		return array(
      'text' => array(
        'type' => 'text',
        'label' => 'Shortcode',
      ),
	  );
  }
}

siteorigin_widget_register( 'shortcode-widget', __FILE__, 'Custom_Widget_ShortcodeWidget' );
