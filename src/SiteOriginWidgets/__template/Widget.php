<?php
/*
Widget Name: __WidgetName__
*/

use Theme\Core\Widget as WidgetCore;

class Custom_Widget___WidgetClassName__ extends WidgetCore 
{
  protected $config;

  public function __construct() 
  {
    $config = array(
      'name' => '__WidgetName__',
      'slug' => '__WidgetSlug__',
      'dir'  => '__WidgetSlug__',
      'file' => __FILE__  
    );

    parent::__construct($config);
	}

  /**
   * Find all available fields here: https://siteorigin.com/docs/widgets-bundle/form-building/form-fields/
   */
  public function get_widget_form() 
  {
		return array(
      'text' => array(
        'type' => 'text',
        'label' => 'Sample Text',
      ),
	  );
  }
}

siteorigin_widget_register( '__WidgetSlug__', __FILE__, 'Custom_Widget___WidgetClassName__' );
