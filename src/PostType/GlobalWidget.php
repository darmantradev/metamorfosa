<?php

namespace Theme\PostType;

use Theme\PostType\CustomPostType;

class GlobalWidget extends CustomPostType
{
    protected static $postType = 'global-widget';

    protected static $postName = 'Global Widget';

    protected static $singularName = 'Global Widget';

    protected static $pluralName = 'Global Widgets';

    protected static $supports = array(
        'title',
        'editor'
    );

    protected static $hasArchive = false;
}
