<?php

namespace Theme\Config;

use Theme\Admin\SiteOriginCustomFields\SiteOriginCustomFields as CustomFields;

class SiteOriginConfig 
{
    private const MODULE_DIR = '/src/SiteOriginWidgets';

    public static function register()
    {
        if( class_exists('SiteOrigin_Widgets_Bundle') ) {
            add_filter('siteorigin_widgets_widget_folders', [__CLASS__, 'getWidgetsDir']);
            add_filter('siteorigin_panels_widget_dialog_tabs', [__CLASS__, 'createWidgetTab']);
            add_filter('siteorigin_widgets_active_widgets', [__CLASS__, 'activeWidgets']);
            new CustomFields();
        }
    }

    public static function getWidgetsDir( $folders )
    {
        $folders[] = get_template_directory() . '/src/SiteOriginWidgets/';

        return $folders;
    }

    public static function createWidgetTab( $tabs )
    {
        $tabs[] = array(
            'title'     => 'Theme Widgets',
            'filter'    => array(
                'groups' => array('theme-widget')
            )
        );

        return $tabs;
    }

    public static function activeWidgets( $widgets )
    {
        $widget_dir = preg_grep('/^([^.])/', scandir(get_template_directory() . self::MODULE_DIR));

        foreach( $widget_dir as $dir ) {
            if( is_dir(get_template_directory() . self::MODULE_DIR . '/' . $dir) && $dir != '__template' ) {
                $widgets[$dir] = true;
            }
        }

        return $widgets;
    }
}