<?php
namespace Theme\Config;

class ThemeSetup
{
    public static function register()
    {
        add_action( 'after_setup_theme', [__CLASS__, 'loadTextDomain'] );
        add_filter( 'body_class', [__CLASS__, 'addBodyClass'] );
    }

    public static function loadTextDomain()
    {
        load_theme_textdomain( $_ENV['TEXT_DOMAIN'], get_template_directory() . '/languages' );
    }

    public static function addBodyClass( $classes )
    {
        $show_banner = get_field('show_banner');

        if( !$show_banner ) {
            $classes = array_merge( $classes, array('no-banner') );
        }

        return $classes;
    }
}
?>