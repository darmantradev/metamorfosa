<?php

namespace Theme\Config;

class Widget 
{
    public static function register()
    {
        add_action( 'widgets_init', array( __CLASS__, 'widget_location' ) );
    }

    public static function widget_location()
    {
        register_sidebar(
            array(
                'name'          => esc_html__( 'Sidebar' ),
                'id'            => 'sidebar-1',
                'description'   => esc_html__( 'Add widgets here.' ),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            )
        );
    }
}