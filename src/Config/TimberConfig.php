<?php 
namespace Theme\Config;

use Timber\Timber;
use Timber\Menu;

use Theme\Config\Menus;
use Theme\Admin\ThemeOptions\ThemeOptions;
use Theme\Admin\ThemeOptions\ThemeOptionsFields;

class TimberConfig
{
    public function __construct()
    {
        $this->register();
    }

    public function register()
    {
        if ( class_exists( 'Timber' ) ) {
            $this->setTwigDirectory();
            add_filter( 'timber/context', array( $this, 'addToContext' ) );
		    add_filter( 'timber/twig', array( $this, 'addToTwig' ) );
        }
    }

    public function setTwigDirectory()
    {
        Timber::$locations = array(
            get_template_directory() . '/src/',
            get_template_directory() . '/src/Admin/',
            get_template_directory() . '/src/Modules/',
            get_template_directory() . '/src/SiteOriginWidgets/'
        );
    }

    public function addToContext( $context )
    {
        $context['theme_menus'] = array();
        foreach (Menus::$menus as $key => $value) {
            $context['theme_menus'][$key] = new Menu($key);
        }

        $context['is_home'] = is_home();
        $context['is_front_page'] = is_front_page();
        $context['is_logged_in'] = is_user_logged_in();

        return $context;
    }

    public function addToTwig( $twig )
    {
        if (function_exists('d')) {
            $twig->addFunction(
                new \Twig_Function(
                    'd',
                    function ($var) {
                        d($var);
                    }
                )
            );
        }
        
        if (function_exists('dd')) {
            $twig->addFunction(
                new \Twig_Function(
                    'dd',
                    function ($var) {
                        dd($var);
                    }
                )
            );
        }

        $twig->addFunction( new \Twig_Function('option', [$this, 'getThemeOption'] ) );
        $twig->addFunction( new \Twig_Function('image_ratio', [$this, 'getImageRatio'] ) );
        $twig->addFilter( new \Twig_Filter('siteorigin_url', [$this, 'siteOriginUrl'] ) );
        $twig->addFilter(new \Twig_SimpleFilter('tel', function ($str) {
            return filter_var($str, FILTER_SANITIZE_NUMBER_FLOAT);
        }));

        return $twig;
    }

    public function getThemeOption($field_name)
    {
        $option = ThemeOptions::get($field_name);

        if( $field_name == 'social_media' ) {
            $option = array();
            $social_media = ThemeOptionsFields::getSocialMedia();

            foreach ($social_media as $key => $value) {
                if( ThemeOptions::get('enable_' . $key) == '1' ) {
                    $option[$key] = array(
                        'url'           => ThemeOptions::get($key . '_url'),
                        'logo_type'     => ThemeOptions::get($key . '_icon_source'),
                        'logo'          => ThemeOptions::get($key . '_icon_source') == 'font-icon' ? ThemeOptions::get($key . '_font_icon') : ThemeOptions::get($key . '_image_icon')
                    );
                }
            }
        }

        return $option;
    }

    public function siteOriginUrl($url)
    {
        if( strpos($url, 'post: ') !== false ) {
            $url = str_replace('post: ', '', $url);
            $url = get_permalink($url);
        }

        return $url;
    }

    public function getImageRatio($image_id) 
    {
        $image = wp_get_attachment_image_src( $image_id, 'full' );

        $ratio = $image[2] / $image[1] * 100;

        return '<i style="padding-bottom: '. $ratio .'%; display: block"></i>';
    }
}
?>