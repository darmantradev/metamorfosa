<?php

namespace Theme\Config;

class ThemeSupport 
{
    public static function register()
    {
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
        );
		add_theme_support( 'customize-selective-refresh-widgets' );
		add_theme_support( 'admin-bar', array( 'callback' => '__return_false' ) );
    }
}