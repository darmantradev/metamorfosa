<?php

namespace Theme\Config;

class Assets 
{
    public static $css = [];

    public static $js = [];

    public static function load()
    {
        static::add_css([
            'enqueue'         => true,
            'handle'          => 'bootstrap-reboot',
            'src'             => '/css/libs/bootstrap-reboot.min.css',
        ]);

        static::add_css([
            'enqueue'         => true,
            'handle'          => 'bootstrap-grid',
            'src'             => '/css/libs/bootstrap-grid.min.css',
            'deps'            => ['bootstrap-reboot']
        ]);

        static::add_css([
            'enqueue'         => true,
            'handle'          => 'lightbox',
            'src'             => '/css/libs/glightbox.min.css',
        ]);

        static::add_css([
            'enqueue'         => true,
            'handle'          => 'theme-style',
            'src'             => '/style.css',
        ]);

        static::add_css([
            'enqueue'         => true,
            'handle'          => 'sal',
            'src'             => '/css/libs/sal.css',
        ]);

        static::add_js([
            'enqueue'         => true,
            'handle'          => 'modernizr',
            'src'             => '/js/libs/modernizr.js',
            'footer'          => false
        ]);

        static::add_js([
            'enqueue'         => true,
            'handle'          => 'lazysizes',
            'src'             => '/js/libs/lazysizes/lazysizes.min.js',
        ]);

        static::add_js([
            'enqueue'         => true,
            'handle'          => 'lazysizes-unveilhooks',
            'deps'            => ['lazysizes'],  
            'src'             => '/js/libs/lazysizes/plugins/unveilhooks/ls.unveilhooks.min.js',
        ]);

        static::add_js([
            'enqueue'         => true,
            'handle'          => 'lightbox',
            'src'             => '/js/libs/glightbox.min.js',
        ]);

        static::add_js([
            'enqueue'         => true,
            'handle'          => 'sal',
            'src'             => '/js/libs/sal.js',
        ]);

        static::add_js([
            'enqueue'         => true,
            'handle'          => 'global',
            'src'             => '/js/global.js',
        ]);

        static::add_js([
            'handle'          => 'social-media-share',
            'src'             => '/js/social-media-share.js',
        ]);

        static::add_js([
            'enqueue'         => true,
            'handle'          => 'sweet-scroll',
            'src'             => '/js/libs/sweet-scroll.min.js',
        ]);

        add_action('wp_enqueue_scripts', __CLASS__.'::register_files', 0);
    }

    public static function add_css( $config )
    {
        $defaults = [
            'enqueue'   => false,
            'handle'    => null,
            'src'       => null,
            'deps'      => [],
            'ver'       => false,
            'media'     => 'all'
        ];

        $config = array_merge( $defaults, $config );

        static::$css[] = $config;
    }

    public static function add_js( $config )
    {
        $defaults = [
            'enqueue'   => false,
            'handle'    => null,
            'src'       => null,
            'deps'      => [],
            'ver'       => false,
            'footer'    => true,
            'data'      => false,
        ];

        $config = array_merge( $defaults, $config );

        static::$js[] = $config;
    }

    public static function register_files()
    {
        // CSS
        if ( static::$css !== [] ) {
            foreach ( static::$css as $css ) {
                if( $css['enqueue'] ) {
                    wp_enqueue_style( $css['handle'] , get_template_directory_uri() . $css['src'], $css['deps'], $css['ver'], $css['media']);
                }else {
                    wp_register_style( $css['handle'] , get_template_directory_uri() . $css['src'], $css['deps'], $css['ver'], $css['media']);
                }
            }
        }

        // JS
        if ( static::$js !== array() ) {
            foreach ( static::$js as $js ) {
                if( $js['enqueue'] ) {
                    wp_enqueue_script( $js['handle'] , get_template_directory_uri() . $js['src'], $js['deps'], $js['ver'], $js['footer']);
                }else {
                    wp_register_script( $js['handle'] , get_template_directory_uri() . $js['src'], $js['deps'], $js['ver'], $js['footer']);
                }

                if ( $js['data'] ) {
                    wp_localize_script($js['handle'], $js['data']['name'], $js['data']['config']);

                    if( $js['enqueue'] ) {
                        wp_enqueue_script($js['handle']);
                    }else {
                        wp_register_script($js['handle']);
                    }
                }
            }
        }
    }
}