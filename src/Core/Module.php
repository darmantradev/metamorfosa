<?php

namespace Theme\Core;

use Timber\Timber;

class Module 
{
    private const MODULE_DIR = '/src/Modules';
    private $name;
    private $config;

    public function init()
    {
        $this->setConfig( $this->defaultConfig() );
        add_action('wp_enqueue_scripts', [$this, 'registerStyle']);
        add_action('wp_enqueue_scripts', [$this, 'registerScript']);
        add_shortcode('md-' . $this->getDashCaseName(), [$this, 'renderModule']);
    }

    public function defaultConfig()
    {
        $config = array(
            'global_css' => false,
            'attr'       => array(), // Shortcode attributes
            'context'    => array() // Pass Variable to template
        );

        return $config;
    }

    /**
     * Register all modules
     */
    public static function register() 
    {
        $modules = preg_grep('/^([^.])/', scandir(get_template_directory() . self::MODULE_DIR));

        foreach ($modules as $namespace) {
            if( is_dir(get_template_directory() . self::MODULE_DIR . '/' . $namespace) && $namespace != '__template' ) {
                $classname = '\\Theme\\Modules\\'.$namespace.'\\Module';
                if( class_exists($classname) ) {
                    $module = new $classname();
                    $module->setName($namespace);
                    $module->init();
                    $module->processModule();
                }
            }
        }
    }
    
    public function processModule()
    {

    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setConfig($config)
    {
        $final_config = array_merge( $this->defaultConfig(), $config );
        $this->config = $final_config;
    }

    public function getConfig() 
    {
        return $this->config;
    }

    public function getTemplateName($attr)
    {
        return 'template';
    }

    public function getTemplate($attr = array())
    {
        $context = Timber::get_context();
        $post = new \Timber\Post();

        $context['attr'] = $attr;
        $context['post'] = $post;

        foreach ($this->getConfig()['context'] as $key => $value) {
            $context[$key] = $value;
        }

        return Timber::compile($this->getName() . '/' . $this->getTemplateName($attr) . '.twig', $context);
    }

    public function renderModule($atts)
    {   
        if( !$this->getConfig()['global_css'] ) {
            wp_enqueue_style($this->getName());
        }

        wp_enqueue_script($this->getName());

        $attr = shortcode_atts( $this->getConfig()['attr'], $atts );

        return $this->getTemplate($attr);
    }

    /**
     * TODO enqueue other styles from child class
     */
    public function registerStyle()
    {
        $filename = get_template_directory() . self::MODULE_DIR . "/{$this->getName()}/style.css";

        if( ! wp_style_is($this->getName()) && file_exists($filename) ) {
            wp_register_style($this->getName(), get_template_directory_uri() . self::MODULE_DIR . "/{$this->getName()}/style.css");

            if( $this->getConfig()['global_css'] ) {
                wp_enqueue_style($this->getName());
            }
        }
    }

    /**
     * TODO enqueue other scripts from child class
     */
    public function registerScript()
    {
        $filename = get_template_directory() . self::MODULE_DIR . "/{$this->getName()}/scripts.js";

        if( ! wp_script_is($this->getName()) && file_exists($filename) ) {
            wp_register_script($this->getName(), get_template_directory_uri() . self::MODULE_DIR . "/{$this->getName()}/scripts.js", null, false, true);
        }
    }

    public function getDashCaseName()
    {
        return strtolower(preg_replace('%([a-z])([A-Z])%', '\1-\2', $this->getName()));
    }
}