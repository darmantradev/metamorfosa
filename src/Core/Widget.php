<?php

namespace Theme\Core;

class Widget extends \SiteOrigin_Widget
{
    protected $dir_uri;
    protected $config;

    public function __construct($config)
    {
        $this->dir_uri = get_template_directory_uri() . '/src/SiteOriginWidgets';

        $this->config = $config;

        parent::__construct(
			$this->config['slug'],
			$this->config['name'],
			array(
				'description'   => null,
                'panels_groups' => array('theme-widget')
			),
			array(),
			false,
			plugin_dir_path( $this->config['file'] )
        );
    }
    
    public function initialize()
    {
        add_filter('siteorigin_widgets_active_widgets', [$this, 'activeWidgets']);
        $this->registerAssets();
    }

    public function registerAssets()
    {
        $this->register_frontend_scripts(
            array(
              array( $this->config['slug'], $this->dir_uri . '/' . $this->config['dir'] . '/scripts.js', null, false, true )
            )
        );
    
        $this->register_frontend_styles(
            array(
              array( $this->config['slug'], $this->dir_uri . '/' . $this->config['dir'] . '/style.css' )
            )
        );
    }

    public function activeWidget( $widgets )
    {
        $widgets[$this->config['slug']] = true;
        return $widgets;
    }
}