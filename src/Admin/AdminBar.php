<?php
namespace Theme\Admin;

class AdminBar
{
    public static function register()
    {
        if ( is_admin_bar_showing() ) {
            add_action('wp_enqueue_scripts', [__CLASS__, 'enqueueJS']);
            add_action( 'wp_head', [__CLASS__, 'adminBarCSS'] );
            add_action( 'wp_footer', [__CLASS__, 'adminBarToggle'] );
            add_filter('autoptimize_filter_css_exclude', [__CLASS__, 'excludeInAutoptimize'], 10, 1);
        }
    }

    public static function enqueueJS()
    {
        wp_enqueue_script( 'admin-bar-toggle', get_template_directory_uri() . '/js/admin-bar.js', null, false, true );
    }

    public static function adminBarToggle()
    {
        echo '<a href="javascript:void(0)" id="wpadminbar-toggle" title="Toggle Admin bar"><span class="dashicons dashicons-arrow-up-alt2"></span></a>';
    }

    public static function adminBarCSS()
    {
        ob_start();
            echo '<style type="text/css">';
                require_once get_template_directory() . '/css/admin-bar.css';
            echo '</style>';
        echo ob_get_clean();
    }

    public static function excludeInAutoptimize($exclude)
    {
        return $exclude.", #wpadminbar";
    }
}
?>