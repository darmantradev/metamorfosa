<?php
namespace Theme\Admin\ThemeOptions;

use Redux;

class ThemeOptionsFields
{
    public static function getAllSections()
    {
        $opt_name = $_ENV['OPT_NAME'];

        /*
        * Navigation
        */
        Redux::set_section( $opt_name, array(
            'title'            => __( 'Navigation', $_ENV['TEXT_DOMAIN'] ),
            'id'               => 'navigation',
            'customizer_width' => '400px',
            'icon'             => 'el el-lines',
            'fields'           => array(
                array(
                    'id'       => 'navbar_logo',
                    'type'     => 'media',
                    'url'      => true,
                    'title'    => __( 'Navigation Logo', $_ENV['TEXT_DOMAIN'] ),
                    'subtitle' => __( 'navbar_logo', $_ENV['TEXT_DOMAIN'] ),
                    'compiler' => 'true',
                ),
            )
        ) );

        /*
        * Social Media
        */
        Redux::set_section( $opt_name, array(
            'title'            => __( 'Social Media', $_ENV['TEXT_DOMAIN'] ),
            'id'               => 'social_media',
            'customizer_width' => '400px',
            'icon'             => 'el el-facebook',
            'fields'           => self::getSocialMediaFields()
        ) );

        /*
        * Sharing Options
        */
        Redux::set_section( $opt_name, array(
            'title'            => __( 'Social Media Share', $_ENV['TEXT_DOMAIN'] ),
            'id'               => 'social_sharing',
            'customizer_width' => '400px',
            'icon'             => 'el el-share',
            'fields'           => self::getSocialMediaSharingFields()
        ) );

        /*
        * Contact Details
        */
        Redux::set_section( $opt_name, array(
            'title'            => __( 'Contact Details', $_ENV['TEXT_DOMAIN'] ),
            'id'               => 'contact_details',
            'customizer_width' => '400px',
            'icon'             => 'el el-address-book',
            'fields'           => array(
                array(
                    'id'       => 'company_name',
                    'type'     => 'text',
                    'title'    => __('Company Name', $_ENV['TEXT_DOMAIN']),
                    'subtitle' => __('company_name', $_ENV['TEXT_DOMAIN']),
                ),
                array(
                    'id'       => 'email',
                    'type'     => 'text',
                    'title'    => __('Email Address', $_ENV['TEXT_DOMAIN']),
                    'subtitle' => __('email', $_ENV['TEXT_DOMAIN']),
                ),
                array(
                    'id'       => 'phone',
                    'type'     => 'text',
                    'title'    => __('Phone Number', $_ENV['TEXT_DOMAIN']),
                    'subtitle' => __('phone', $_ENV['TEXT_DOMAIN']),
                ),
                array(
                    'id'       => 'address',
                    'type'     => 'textarea',
                    'title'    => __('Location Address', $_ENV['TEXT_DOMAIN']),
                    'subtitle' => __('address', $_ENV['TEXT_DOMAIN']),
                ),
            )

        ) );
    }

    public static function getSocialMedia()
    {
        $social_media = array(
            'facebook' => array(
                'name'          => 'Facebook',
                'default_url'   => 'https://facebook.com',
                'default_icon'  => 'la-facebook-f'
            ),
            'twitter' => array(
                'name'          => 'Twitter',
                'default_url'   => 'https://twitter.com',
                'default_icon'  => 'la-twitter'
            ),
            'instagram' => array(
                'name'          => 'Instagram',
                'default_url'   => 'https://instagram.com',
                'default_icon'  => 'la-instagram'
            ),
            'youtube' => array(
                'name'          => 'Youtube',
                'default_url'   => 'https://youtube.com',
                'default_icon'  => 'la-youtube'
            ),
            'linkedin' => array(
                'name'          => 'Linkedin',
                'default_url'   => 'https://linkedin.com',
                'default_icon'  => 'la-linkedin-in'
            ),
            'pinterest' => array(
                'name'          => 'Pinterest',
                'default_url'   => 'https://pinterest.com',
                'default_icon'  => 'la-pinterest'
            ),
            'whatsapp' => array(
                'name'          => 'WhatsApp',
                'default_url'   => 'https://wa.me/<number>',
                'default_icon'  => 'la-whatsapp'
            ),
            'skype' => array(
                'name'          => 'Skype',
                'default_url'   => 'skype:username?call',
                'default_icon'  => 'la-skype'
            ),
            'tripadvisor' => array(
                'name'          => 'TripAdvisor',
                'default_url'   => 'https://www.tripadvisor.com/',
                'default_icon'  => 'la-tripadvisor'
            ),
        );
    
        return $social_media;
    }

    public static function getSocialMediaFields()
    {
        $fields = array();
        foreach ( self::getSocialMedia() as $id => $social) {
            array_push(
            $fields,
            array(
                'id'       => sprintf('enable_%s', $id),
                'type'     => 'switch',
                'title'    => __( sprintf('Enable %s', $social['name']), $_ENV['TEXT_DOMAIN']),
                'default'  => false
            )
            );

            array_push(
            $fields,
            array(
                'id'       => sprintf('%s_url', $id),
                'type'     => 'text',
                'title'    => __( sprintf('%s URL', $social['name']), $_ENV['TEXT_DOMAIN']),
                'subtitle' => __( sprintf('%s_url', $id), $_ENV['TEXT_DOMAIN']),
                'default'  => $social['default_url'],
                'required' => array( sprintf('enable_%s', $id), '=', '1' ),
            )
            );

            array_push(
            $fields,
            array(
                'id'       => sprintf('%s_icon_source', $id),
                'type'     => 'radio',
                'title'    => __('Icon source', $_ENV['TEXT_DOMAIN']),
                'subtitle' => __( sprintf('%s_icon_source', $id), $_ENV['TEXT_DOMAIN']),
                'required' => array( sprintf('enable_%s', $id), '=', '1' ),
                'options'  => array(
                    'font-icon' => 'Font icon',
                    'image' => 'Image (Media library)',
                ),
                'default' => 'font-icon'
            )
            );

            array_push(
            $fields,
            array(
                'id'       => sprintf('%s_font_icon', $id),
                'type'     => 'text',
                'title'    => __('Font Icon', $_ENV['TEXT_DOMAIN']),
                'subtitle' => __( sprintf('%s_font_icon', $id), $_ENV['TEXT_DOMAIN']),
                'default'  => $social['default_icon'],
                'required' => array( sprintf('%s_icon_source', $id), '=', 'font-icon' ),
            )
            );

            array_push(
            $fields,
            array(
                'id'       => sprintf('%s_image_icon', $id),
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Image icon', $_ENV['TEXT_DOMAIN'] ),
                'subtitle' => __( sprintf('%s_image_icon', $id), $_ENV['TEXT_DOMAIN'] ),
                'required' => array( sprintf('%s_icon_source', $id), '=', 'image' ),
            )
            );
        }

        return $fields;
    }

    public static function getSocialMediaShare( $url = '', $title = '', $text = '', $thumb = '' )
    {
        $social_media = array(
            'facebook_share'  => array(
              'name'          => 'Facebook',
              'url'           => 'https://www.facebook.com/sharer.php?u=' . $url,
              'default_icon'  => 'la-facebook-f',
              'popup'         => true,
              'target'        => '_blank'
            ),
            'twitter_share'   => array(
              'name'          => 'Twitter',
              'url'           => 'https://twitter.com/intent/tweet?url=' . $url . '&text=' . $text,
              'default_icon'  => 'la-twitter',
              'popup'         => true,
              'target'        => '_blank'
            ),
            'linkedin_share'  => array(
              'name'          => 'Linkedin',
              'url'           => 'https://www.linkedin.com/shareArticle?mini=true&url=' . $url . '&title=' . $title . '&summary=' . $text,
              'default_icon'  => 'la-linkedin-in',
              'popup'         => true,
              'target'        => '_blank'
            ),
            'pinterest_share' => array(
              'name'          => 'Pinterest',
              'url'           => 'https://pinterest.com/pin/create/button/?url=' . $url . '&media=' . $thumb . '&description=' . $text,
              'default_icon'  => 'la-pinterest',
              'popup'         => true,
              'target'        => '_blank'
            ),
            'whatsapp_share'  => array(
              'name'          => 'WhatsApp',
              'url'           => 'https://wa.me/?text=' .$text,
              'default_icon'  => 'la-whatsapp',
              'popup'         => false,
              'target'        => '_blank'
            ),
            'email_share'     => array(
              'name'          => 'Email',
              'url'           => 'mailto:?subject=' . $title . '&body=' . $text,
              'default_icon'  => 'la-envelope',
              'popup'         => false,
              'target'        => ''
            ),
            'copy_link'   => array(
              'name'          => 'Copy link',
              'url'           => get_permalink(),
              'default_icon'  => 'la-link',
              'popup'         => false,
              'title'         => 'Copy link',
              'target'        => ''
            ),
          );
        
          return $social_media;
    }

    public static function getSocialMediaSharingFields()
    {
        $fields = array();
        foreach ( self::getSocialMediaShare() as $id => $social) {
            array_push(
            $fields,
            array(
                'id'       => sprintf('enable_%s', $id),
                'type'     => 'switch',
                'title'    => __( sprintf('Enable %s', $social['name']), $_ENV['TEXT_DOMAIN']),
                'default'  => false
            )
            );

            array_push(
            $fields,
            array(
                'id'       => sprintf('%s_icon_source', $id),
                'type'     => 'radio',
                'title'    => __('Icon source', $_ENV['TEXT_DOMAIN']),
                'subtitle' => __( sprintf('%s_icon_source', $id), $_ENV['TEXT_DOMAIN']),
                'required' => array( sprintf('enable_%s', $id), '=', '1' ),
                'options'  => array(
                    'font-icon' => 'Font icon',
                    'image' => 'Image (Media library)',
                ),
                'default' => 'font-icon'
            )
            );

            array_push(
            $fields,
            array(
                'id'       => sprintf('%s_font_icon', $id),
                'type'     => 'text',
                'title'    => __('Font Icon', $_ENV['TEXT_DOMAIN']),
                'subtitle' => __( sprintf('%s_font_icon', $id), $_ENV['TEXT_DOMAIN']),
                'default'  => $social['default_icon'],
                'required' => array( sprintf('%s_icon_source', $id), '=', 'font-icon' ),
            )
            );

            array_push(
            $fields,
            array(
                'id'       => sprintf('%s_image_icon', $id),
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Image icon', $_ENV['TEXT_DOMAIN'] ),
                'subtitle' => __( sprintf('%s_image_icon', $id), $_ENV['TEXT_DOMAIN'] ),
                'required' => array( sprintf('%s_icon_source', $id), '=', 'image' ),
            )
            );
        }

        return $fields;
    }
}