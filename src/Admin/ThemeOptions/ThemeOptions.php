<?php
namespace Theme\Admin\ThemeOptions;

use Redux;
use Theme\Admin\ThemeOptions\ThemeOptionsFields;

class ThemeOptions 
{
    public function __construct()
    {
        $this->register();
    }

    public function register()
    {
        if ( class_exists( 'Redux' ) ) {
            Redux::disable_demo();
            add_action("redux/extensions/{$_ENV['OPT_NAME']}/before", [$this, 'loadExtensions'], 0);
            $this->getConfig();
            Redux::init($_ENV['OPT_NAME']);
        } else {
            return new \WP_Error( 'Redux plugin is not activated', __( 'Please install and activate Redux plugin', $_ENV['TEXT_DOMAIN'] ) );
        }
    }

    public static function get( $field_name )
    {
        global ${$_ENV['OPT_NAME']};

        $options = ${$_ENV['OPT_NAME']};

        if( isset($options[ $field_name ]) ) {
            return $options[ $field_name ];
        }else {
            return null;
        }
    }

    public static function getAll()
    {
        global ${$_ENV['OPT_NAME']};

        $options = ${$_ENV['OPT_NAME']};

        return $options;
    }

    public function loadExtensions($ReduxFramework)
    {
        $path = dirname( __FILE__ ) . '/extensions/';
        $folders = scandir( $path, 1 );
        foreach ( $folders as $folder ) {
            if ( $folder === '.' or $folder === '..' or ! is_dir( $path . $folder ) ) {
                continue;
            }
            $extension_class = 'ReduxFramework_Extension_' . $folder;
            if ( ! class_exists( $extension_class ) ) {
                $class_file = $path . $folder . '/extension_' . $folder . '.php';
                $class_file = apply_filters( 'redux/extension/' . $ReduxFramework->args['opt_name'] . '/' . $folder, $class_file );
                if ( $class_file ) {
                    require_once( $class_file );
                }
            }
            if ( ! isset( $ReduxFramework->extensions[ $folder ] ) ) {
                $ReduxFramework->extensions[ $folder ] = new $extension_class( $ReduxFramework );
            }
        }
    }

    public function getConfig()
    {
        $args = array(
            'opt_name'              => $_ENV['OPT_NAME'],
            'display_name'          => __( 'Theme Settings', $_ENV['TEXT_DOMAIN'] ),
            'menu_title'            => __( 'Theme Settings', $_ENV['TEXT_DOMAIN'] ),
            'page_title'            => __( 'Theme Settings', $_ENV['TEXT_DOMAIN'] ),
            'dev_mode'              => $_ENV['WP_ENV'] == 'development' ? true : false,
            'page_permissions'      => 'manage_options',
            'show_options_object'   => false   
        );

        Redux::set_args( $_ENV['OPT_NAME'], $args );
        ThemeOptionsFields::getAllSections();
    }
}