/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/dev-generator.js":
/*!*****************************!*\
  !*** ./js/dev-generator.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

var devGenerator = {
  init: function init() {
    var form = document.getElementById('dev-generator');

    if (!document.body.contains(form)) {
      return false;
    }

    form.addEventListener('submit', function (e) {
      e.preventDefault();
      devGenerator.runAjax(e.target);
    });
  },
  runAjax: function runAjax(form) {
    var type = form.querySelector('select[name="type"]').value,
        url = dev_options_object.ajaxurl;
    var data = new FormData(form);

    switch (type) {
      case 'module':
        data.append('action', 'generate_module');
        break;

      case 'widget':
        data.append('action', 'generate_widget');
        break;
    }

    data.append('nonce', dev_options_object.nonce);
    var fetchData = {
      method: 'POST',
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      credentials: 'same-origin',
      body: data
    };
    fetch(url, fetchData).then(function (response) {
      return response.json();
    }).then(function (data) {})["catch"](function (err) {
      console.log(err);
    });
  }
};
devGenerator.init();

/***/ }),

/***/ "./js/font-loader.js":
/*!***************************!*\
  !*** ./js/font-loader.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

var fontLoader = {
  init: function init() {
    var wrapper = document.querySelector('.fl-wrapper'),
        modal = document.querySelector('.fl-modal-wrapper');

    if (!document.body.contains(wrapper)) {
      return false;
    }

    fontLoader.initUploader();
    document.body.addEventListener('click', function (e) {
      if (e.target && e.target.classList.contains('fl-btn-preview')) {
        e.preventDefault();
        var demo = e.target.getAttribute('data-url');
        modal.classList.add('active');
        modal.querySelector('iframe').setAttribute('src', demo);
      }

      if (e.target && e.target.classList.contains('fl-btn-delete')) {
        e.preventDefault();
        var folder_name = e.target.getAttribute('data-folder-name');
        var r = confirm('Are you sure want to delete this font');

        if (r == true) {
          fontLoader.removeDir(e.target, folder_name);
        }
      }
    });
    document.body.addEventListener('change', function (e) {
      if (e.target && e.target.classList.contains('fl-checkbox')) {
        fontLoader.statusSwitchToggle(e.target);
      }
    });
    modal.querySelector('.fl-close-modal').addEventListener('click', function (e) {
      e.preventDefault();
      modal.classList.remove('active');
      modal.querySelector('iframe').setAttribute('src', '');
    });
  },
  initUploader: function initUploader() {
    var uploader = new plupload.Uploader({
      browse_button: 'fl-browse',
      url: dev_options_object.ajaxurl,
      drop_element: 'fl-dropzone',
      filters: {
        mime_types: [{
          title: 'Zip files',
          extensions: 'zip'
        }],
        max_file_size: dev_options_object.max_upload_size,
        prevent_duplicates: true
      },
      multipart_params: {
        action: 'upload_file',
        nonce: dev_options_object.nonce
      }
    });
    uploader.init();
    uploader.bind('FilesAdded', function (up, files) {
      var html = '';
      plupload.each(files, function (file) {
        html += fontLoader.uploadItemTemplate(file.id, file.name, plupload.formatSize(file.size));
      });
      var uploadWrapper = document.querySelector('.fl-uploaded-wrapper');
      uploadWrapper.insertAdjacentHTML('afterbegin', html);
      uploader.start();
    });
    uploader.bind('UploadProgress', function (up, file) {
      document.getElementById(file.id).querySelector('.fl-upload-progress-number').style.width = file.percent + '%';
    });
    uploader.bind('FileUploaded', function (up, file, result) {
      var response = JSON.parse(result.response);

      if (response.error == 0) {
        var wrapper = document.querySelector('.fl-uploaded-wrapper');
        document.getElementById(file.id).classList.add('hide');
        document.getElementById(file.id).addEventListener('transitionend', function (e) {
          if (e.propertyName === 'opacity') {
            document.getElementById(file.id).remove();
            Object.keys(response.meta).forEach(function (key) {
              var template = "\n                                <div class=\"fl-item\">\n                                    <div class=\"fl-item-left\">\n                                        <p class=\"fl-font-name\">".concat(response.meta[key].name, "</p>\n                                        <span class=\"fl-font-weight\">Font Weight: ").concat(response.meta[key].weight, "</span>\n                                        <ul class=\"fl-more-options\">\n                                            <li><a href=\"javascript:void(0)\" class=\"fl-btn-preview\" data-url=\"").concat(response.meta[key].font_uri + '/' + key + '/' + response.meta[key].demo, "\">preview</a></li>\n                                            <li><a href=\"javascript:void(0)\" class=\"fl-btn-delete\" data-folder-name=\"").concat(response.meta[key].font_dir, "\">delete</a></li>\n                                        </ul>\n                                    </div>\n                                    <div class=\"fl-item-right\">\n                                        <ul>\n                                            <li><label class=\"fl-switch\"><input type=\"checkbox\" class=\"fl-checkbox\"><div class=\"slider round\"></div></label></li>\n                                        </ul>\n                                    </div>\n                                </div>\n                            ");
              wrapper.insertAdjacentHTML('afterbegin', template);
            });
          }
        });
      }
    });
    uploader.bind('Error', function (up, err) {
      document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
    });
  },
  uploadItemTemplate: function uploadItemTemplate(id, name, size) {
    var template = "<div class=\"fl-upload-item\" id=\"".concat(id, "\">\n            <div class=\"fl-upload-meta-wrapper\">\n                <div class=\"fl-upload-icon\">\n                <i class=\"dashicons dashicons-media-archive\"></i>\n                </div>\n                <div class=\"fl-upload-meta\">\n                <p class=\"fl-upload-file-name\">").concat(name, "</p>\n                <span class=\"fl-upload-file-size\">").concat(size, "</span>\n                </div>\n            </div>\n            <div class=\"fl-upload-progress-wrapper\">\n                <span class=\"fl-upload-progress-rail\">\n                <span class=\"fl-upload-progress-number\" style=\"width:0%\"></span>\n                </span>\n            </div>\n            </div>");
    return template;
  },
  removeDir: function removeDir(e, folder) {
    var url = dev_options_object.ajaxurl;
    var data = new FormData();
    data.append('action', 'delete_folder');
    data.append('nonce', dev_options_object.nonce);
    data.append('folder_name', folder);
    var fetchData = {
      method: 'POST',
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      credentials: 'same-origin',
      body: data
    };
    fetch(url, fetchData).then(function (response) {
      return response.json();
    }).then(function (data) {
      if (data.error === 0) {
        e.closest('.fl-item').remove();
      }
    })["catch"](function (err) {
      console.log(err);
    });
  },
  statusSwitchToggle: function statusSwitchToggle(e) {
    var folder_name = e.getAttribute('data-folder-name');

    if (e.checked) {
      e.closest('.fl-item').classList.remove('fl-disabled');
      e.closest('.fl-item').classList.add('fl-enabled');
      fontLoader.changeStatus(folder_name, 'add');
    } else {
      e.closest('.fl-item').classList.remove('fl-enabled');
      e.closest('.fl-item').classList.add('fl-disabled');
      fontLoader.changeStatus(folder_name, 'remove');
    }
  },
  changeStatus: function changeStatus(folder, status) {
    var url = dev_options_object.ajaxurl;
    var data = new FormData();
    data.append('action', 'change_status');
    data.append('nonce', dev_options_object.nonce);
    data.append('folder_name', folder);
    data.append('status', status);
    var fetchData = {
      method: 'POST',
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      credentials: 'same-origin',
      body: data
    };
    fetch(url, fetchData).then(function (response) {
      return response.json();
    }).then(function (data) {})["catch"](function (err) {
      console.log(err);
    });
  }
};
fontLoader.init();

/***/ }),

/***/ "./js/main.js":
/*!********************!*\
  !*** ./js/main.js ***!
  \********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _dev_generator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dev-generator */ "./js/dev-generator.js");
/* harmony import */ var _dev_generator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_dev_generator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _font_loader__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./font-loader */ "./js/font-loader.js");
/* harmony import */ var _font_loader__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_font_loader__WEBPACK_IMPORTED_MODULE_1__);



/***/ }),

/***/ "./scss/main.scss":
/*!************************!*\
  !*** ./scss/main.scss ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*******************************************!*\
  !*** multi ./js/main.js ./scss/main.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\laragon\www\themedev\wp-content\themes\theme-base\src\Admin\DeveloperOptions\js\main.js */"./js/main.js");
module.exports = __webpack_require__(/*! C:\laragon\www\themedev\wp-content\themes\theme-base\src\Admin\DeveloperOptions\scss\main.scss */"./scss/main.scss");


/***/ })

/******/ });