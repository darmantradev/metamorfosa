const fontLoader = {
    init: () => {
        const wrapper = document.querySelector('.fl-wrapper'),
              modal = document.querySelector('.fl-modal-wrapper')

        if (!document.body.contains(wrapper)) {
            return false;
        }

        fontLoader.initUploader();

        document.body.addEventListener('click', e => {
            if( e.target && e.target.classList.contains('fl-btn-preview') ) {
                e.preventDefault();

                const demo = e.target.getAttribute('data-url');
                modal.classList.add('active');
                modal.querySelector('iframe').setAttribute('src', demo);
            }

            if( e.target && e.target.classList.contains('fl-btn-delete') ) {
                e.preventDefault();

                const folder_name = e.target.getAttribute('data-folder-name');
                var r = confirm('Are you sure want to delete this font');
                if( r == true ) {
                  fontLoader.removeDir( e.target, folder_name );
                }
            }
        });

        document.body.addEventListener('change', e => {
            if( e.target && e.target.classList.contains('fl-checkbox') ) {
                fontLoader.statusSwitchToggle( e.target );
            }
        });

        modal.querySelector('.fl-close-modal').addEventListener('click', e => {
            e.preventDefault();
            modal.classList.remove('active');
            modal.querySelector('iframe').setAttribute('src', '');
        });
    },
    initUploader: () => {
        const uploader = new plupload.Uploader({
            browse_button: 'fl-browse',
            url: dev_options_object.ajaxurl,
            drop_element: 'fl-dropzone',
            filters: {
                mime_types: [{
                    title: 'Zip files',
                    extensions: 'zip'
                }],
                max_file_size: dev_options_object.max_upload_size,
                prevent_duplicates: true
            },
            multipart_params: {
                action: 'upload_file',
                nonce: dev_options_object.nonce
            }
        });

        uploader.init();

        uploader.bind('FilesAdded', function (up, files) {
            let html = '';

            plupload.each(files, function (file) {
                html += fontLoader.uploadItemTemplate(file.id, file.name, plupload.formatSize(file.size));
            });

            const uploadWrapper = document.querySelector('.fl-uploaded-wrapper');
            uploadWrapper.insertAdjacentHTML('afterbegin', html);

            uploader.start();
        });

        uploader.bind('UploadProgress', function (up, file) {
            document.getElementById(file.id).querySelector('.fl-upload-progress-number').style.width = file.percent + '%';
        });

        uploader.bind('FileUploaded', function (up, file, result) {
            let response = JSON.parse(result.response);
            if (response.error == 0) {

                const wrapper = document.querySelector('.fl-uploaded-wrapper');

                document.getElementById(file.id).classList.add('hide');

                document.getElementById(file.id).addEventListener('transitionend', (e) => {
                    if( e.propertyName === 'opacity' ) {
                        document.getElementById(file.id).remove();
                        Object.keys(response.meta).forEach(key => {
                            let template = `
                                <div class="fl-item">
                                    <div class="fl-item-left">
                                        <p class="fl-font-name">${response.meta[key].name}</p>
                                        <span class="fl-font-weight">Font Weight: ${response.meta[key].weight}</span>
                                        <ul class="fl-more-options">
                                            <li><a href="javascript:void(0)" class="fl-btn-preview" data-url="${ response.meta[key].font_uri + '/' + key + '/' + response.meta[key].demo }">preview</a></li>
                                            <li><a href="javascript:void(0)" class="fl-btn-delete" data-folder-name="${response.meta[key].font_dir}">delete</a></li>
                                        </ul>
                                    </div>
                                    <div class="fl-item-right">
                                        <ul>
                                            <li><label class="fl-switch"><input type="checkbox" class="fl-checkbox"><div class="slider round"></div></label></li>
                                        </ul>
                                    </div>
                                </div>
                            `;
        
                            wrapper.insertAdjacentHTML('afterbegin', template);
                        });
                    }
                });
            }
        });

        uploader.bind('Error', function (up, err) {
            document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
        });
    },
    uploadItemTemplate: (id, name, size) => {
        let template = `<div class="fl-upload-item" id="${id}">
            <div class="fl-upload-meta-wrapper">
                <div class="fl-upload-icon">
                <i class="dashicons dashicons-media-archive"></i>
                </div>
                <div class="fl-upload-meta">
                <p class="fl-upload-file-name">${name}</p>
                <span class="fl-upload-file-size">${size}</span>
                </div>
            </div>
            <div class="fl-upload-progress-wrapper">
                <span class="fl-upload-progress-rail">
                <span class="fl-upload-progress-number" style="width:0%"></span>
                </span>
            </div>
            </div>`;

        return template;
    },
    removeDir: (e, folder) => {
        const url = dev_options_object.ajaxurl;
        let data = new FormData();

        data.append('action', 'delete_folder');
        data.append('nonce', dev_options_object.nonce);
        data.append('folder_name', folder);

        let fetchData = {
            method: 'POST',
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            credentials: 'same-origin',
            body: data
        };

        fetch(url, fetchData)
            .then(response => {
                return response.json();
            })
            .then(data => {
                if( data.error === 0 ) {
                    e.closest('.fl-item').remove();
                }
            })
            .catch(err => {
                console.log(err);
            });
    },
    statusSwitchToggle: (e) => {
        const folder_name = e.getAttribute('data-folder-name');

        if( e.checked ) {
            e.closest('.fl-item').classList.remove('fl-disabled');
            e.closest('.fl-item').classList.add('fl-enabled');
            fontLoader.changeStatus(folder_name, 'add');
        }else{
            e.closest('.fl-item').classList.remove('fl-enabled');
            e.closest('.fl-item').classList.add('fl-disabled');
            fontLoader.changeStatus(folder_name, 'remove');
        }
    },
    changeStatus: (folder, status) => {
        const url = dev_options_object.ajaxurl;
        let data = new FormData();

        data.append('action', 'change_status');
        data.append('nonce', dev_options_object.nonce);
        data.append('folder_name', folder);
        data.append('status', status);

        let fetchData = {
            method: 'POST',
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            credentials: 'same-origin',
            body: data
        };

        fetch(url, fetchData)
            .then(response => {
                return response.json();
            })
            .then(data => {
                
            })
            .catch(err => {
                console.log(err);
            });
    }
}

fontLoader.init();