const devGenerator = {
    init: () => {
        const form = document.getElementById('dev-generator');

        if( !document.body.contains(form) ) {
            return false;
        }

        form.addEventListener('submit', (e) => {
            e.preventDefault();
            devGenerator.runAjax(e.target);
        });
    },
    runAjax: (form) => {
        const type = form.querySelector('select[name="type"]').value,
              url = dev_options_object.ajaxurl;

        let data = new FormData(form);

        switch (type) {
            case 'module':
                data.append('action', 'generate_module');
                break;
            case 'widget':
                data.append('action', 'generate_widget');
                break;
        }
        
        data.append('nonce', dev_options_object.nonce);

        let fetchData = {
            method: 'POST',
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            credentials: 'same-origin',
            body: data
        };

        fetch(url, fetchData)
            .then(response => {
                return response.json();
            })
            .then(data => {
                
            })
            .catch(err => {
                console.log(err);
        });
    }
}

devGenerator.init();