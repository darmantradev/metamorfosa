<?php
namespace Theme\Admin\DeveloperOptions\inc;

class ModuleGenerator 
{
    protected $module_dir;

    public function __construct() 
    {
        $this->module_dir = get_template_directory() . '/src/Modules';

        add_action('wp_ajax_nopriv_generate_module', [ $this, 'generateModule' ] );
        add_action('wp_ajax_generate_module', [ $this, 'generateModule' ] );
    }

    public function createDirectory( $class_name, $slug ) 
    {
        $path = $this->module_dir . '/' . $class_name;
        $status = false;

        if( wp_mkdir_p( $path ) ) {
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            global $wp_filesystem;
            WP_Filesystem();

            //Create class file
            $content_class = $wp_filesystem->get_contents( $this->module_dir . '/__template/Module.php' );
            $content_class = str_replace( '__ModuleName__', $class_name, $content_class );
            $content_class = str_replace( '__ShortcodeName__', $slug, $content_class );
            $wp_filesystem->put_contents($path . '/Module.php', $content_class);

            //Create JS file
            $content_js = $wp_filesystem->get_contents( $this->module_dir . '/__template/scripts.js' );
            $wp_filesystem->put_contents(
                $path . '/scripts.js',
                $content_js,
                FS_CHMOD_FILE
            );

            //Create css file
            $wp_filesystem->put_contents(
                $path . '/style.css',
                '',
                FS_CHMOD_FILE
            );

            //Create template file
            $content_tpl = $wp_filesystem->get_contents( $this->module_dir . '/__template/template.twig' );
            $wp_filesystem->put_contents(
                $path . '/template.twig',
                $content_tpl,
                FS_CHMOD_FILE
            );
            
            $status = true;
        }

        return $status;
    }

    public function generateModule() 
    {
        $nonce = $_POST['nonce'];
        if ( ! wp_verify_nonce( $nonce, 'dev-options-nonce' ) )
            die ( 'Not allowed' );

        $module_name = $_POST['name'];

        $class_name = $this->convertName( $module_name, 'class' );
        $slug = $this->convertName( $module_name, 'slug' );

        $message = 'Module <strong>' . $class_name . '</strong> successfuly created, find the module here: <strong>' . $this->module_dir . '/' . $class_name . '</strong>';

        if( !$this->createDirectory( $class_name, $slug ) ) {
            $message = 'Error occured when creating a module';
        }

        echo $message;

        die();
    }

    public function convertName($string, $type) 
    {
        $str = sanitize_text_field($string);
        $str = preg_replace('/[^A-Za-z0-9]/', ' ', $str);

        switch ($type) {
            case 'class':
                $str = ucwords($str);
                $str = str_replace(' ', '', $str);
                break;
            case 'slug':
                $str = strtolower($str);
                $str = str_replace(' ', '-', $str);
                break;
        }

        return $str;
    }
}

