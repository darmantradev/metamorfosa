<?php
namespace Theme\Admin\DeveloperOptions\inc;

class WidgetGenerator 
{
    protected $widget_dir;

    public function __construct() 
    {
        $this->widget_dir = get_template_directory() . '/src/SiteOriginWidgets';

        add_action('wp_ajax_nopriv_generate_widget', [ $this, 'generateWidget' ] );
        add_action('wp_ajax_generate_widget', [ $this, 'generateWidget' ] );
    }

    public function createDirectory( $widget_name, $class_name, $slug ) 
    {
        $path = $this->widget_dir . '/' . $slug;
        $status = false;

        if( wp_mkdir_p( $path ) ) {
            wp_mkdir_p( $path . '/tpl' );

            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            global $wp_filesystem;
            WP_Filesystem();

            //Create class file
            $content_class = $wp_filesystem->get_contents( $this->widget_dir . '/__template/Widget.php' );
            $content_class = str_replace( '__WidgetName__', $widget_name, $content_class );
            $content_class = str_replace( '__WidgetClassName__', $class_name, $content_class );
            $content_class = str_replace( '__WidgetSlug__', $slug, $content_class );
            $wp_filesystem->put_contents($path . '/'. $slug .'.php', $content_class);

            //Create JS file
            $content_js = $wp_filesystem->get_contents( $this->widget_dir . '/__template/scripts.js' );
            $wp_filesystem->put_contents(
                $path . '/scripts.js',
                $content_js,
                FS_CHMOD_FILE
            );

            //Create css file
            $wp_filesystem->put_contents(
                $path . '/style.css',
                '',
                FS_CHMOD_FILE
            );

            //Create template file
            $content_tpl = $wp_filesystem->get_contents( $this->widget_dir . '/__template/tpl/default.php' );
            $content_tpl = str_replace( '__WidgetSlug__', $slug, $content_tpl );
            $wp_filesystem->put_contents(
                $path . '/tpl/default.php',
                $content_tpl,
                FS_CHMOD_FILE
            );

            $content_twig = $wp_filesystem->get_contents( $this->widget_dir . '/__template/tpl/template.twig' );
            $wp_filesystem->put_contents(
                $path . '/tpl/template.twig',
                $content_twig,
                FS_CHMOD_FILE
            );
            
            $status = true;
        }

        return $status;
    }

    public function generateWidget() 
    {
        $nonce = $_POST['nonce'];
        if ( ! wp_verify_nonce( $nonce, 'dev-options-nonce' ) )
            die ( 'Not allowed' );

        $widget_name = $_POST['name'];

        $name = $this->convertName( $widget_name, 'name' );
        $class_name = $this->convertName( $widget_name, 'class' );
        $slug = $this->convertName( $widget_name, 'slug' );

        $message = 'Module <strong>' . $class_name . '</strong> successfuly created, find the widget here: <strong>' . $this->widget_dir . '/' . $class_name . '</strong>';

        if( !$this->createDirectory( $name, $class_name, $slug ) ) {
            $message = 'Error occured when creating a widget';
        }

        echo $message;

        die();
    }

    public function convertName($string, $type) 
    {
        $str = sanitize_text_field($string);
        $str = preg_replace('/[^A-Za-z0-9]/', ' ', $str);

        switch ($type) {
            case 'name':
                $str = ucwords($str);
                break;
            case 'class':
                $str = ucwords($str);
                $str = str_replace(' ', '', $str);
                break;
            case 'slug':
                $str = strtolower($str);
                $str = str_replace(' ', '-', $str);
                break;
        }

        return $str;
    }
}

