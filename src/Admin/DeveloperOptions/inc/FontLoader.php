<?php
namespace Theme\Admin\DeveloperOptions\inc;

require_once 'FontLib/Autoloader.php';

class FontLoader 
{
    protected $font_dir;
    protected $font_uri;
    protected $response;
    protected $font_ext;
    protected $option_name;

    function __construct()
    {
        $this->font_dir = get_template_directory() . '/fonts';
        $this->font_uri = get_template_directory_uri() . '/fonts';
        $this->font_ext = array(
            'ttf',
            'eof',
            'woff'
        );
        $this->option_name = 'dev_option_fonts';

        $this->response = array();

        add_action('wp_ajax_upload_file', [$this, 'upload'] );
        add_action('wp_ajax_nopriv_upload_file', [$this, 'upload'] );

        add_action('wp_ajax_delete_folder', [$this, 'deleteDirectory'] );
        add_action('wp_ajax_nopriv_delete_folder', [$this, 'deleteDirectory'] );

        add_action('wp_ajax_change_status', [$this, 'changeStatus'] );
        add_action('wp_ajax_nopriv_change_status', [$this, 'changeStatus'] );

        add_action('wp_enqueue_scripts', [$this, 'enqueueStyles']);
    }

    public function getFontMeta( $uploaded_file = false ) 
    {
        $font_name = array();
        $font_weight = array();
        $other_meta = array();
        $fonts = array();

        $scan = preg_grep('/^([^.])/', scandir( $this->font_dir ));

        if( $uploaded_file ) {
          $scan = array( $uploaded_file );
        }

        foreach($scan as $dir) {
          $files = new \RecursiveDirectoryIterator($this->font_dir.'/'.$dir, \RecursiveDirectoryIterator::SKIP_DOTS);
          $iterator = new \RecursiveIteratorIterator($files);

            if ( is_dir($this->font_dir.'/'.$dir) && glob($this->font_dir.'/'.$dir.'/*.css') ) {
                foreach ($iterator as $fileinfo) {
                    if( in_array($fileinfo->getExtension(), $this->font_ext) ) {
                        $font = \FontLib\Font::load($fileinfo->getPathname());
                        if( $font != null ) {
                            $font->parse();
                            $css = glob( $this->font_dir.'/'.$dir.'/*.css' );
                            $demo = glob( $this->font_dir.'/'.$dir.'/*.html' );
                            $icomoon_json = glob( $this->font_dir.'/'.$dir.'/*.json' );
                            $other_meta[$dir]['css'] = !empty($css) ? basename($css[0]) : false;
                            $other_meta[$dir]['demo'] = !empty($demo) ? basename($demo[0]) : false;
                            $other_meta[$dir]['selection'] = !empty($icomoon_json) ? basename($icomoon_json[0]) : false;
                            $other_meta[$dir]['font_dir'] = $this->font_dir;
                            $other_meta[$dir]['font_uri'] = $this->font_uri;
                            $other_meta[$dir]['status'] = $this->getDirStatus($dir);
                            $font_name[$dir][] = $font->getFontName();
                            $font_weight[$dir][] = $font->getFontWeight();
                        }
                    }
                }
            }
        }

        $fonts = array_merge_recursive( $this->parseFontName( $font_name ), $this->parseFontWeight( $font_weight ), $other_meta );

        return $fonts;
    }

    public function parseFontName( $fonts ) 
    {
        $fonts_tmp = array();
        $font_name = array();
        $font_key = 'word';

        foreach ($fonts as $dir => $font) {
            $fonts_tmp[$dir] = array_unique($font);
            sort($fonts_tmp[$dir]);
        }
        foreach ($fonts_tmp as $dir => $font) {
            foreach ($font as $key => $value) {
                if (strpos($value, $font_key) === false) {
                    $font_key = $value;
                    $font_name[$dir]['name'][] = $value;
                }
            }
            $font_name[$dir]['name'] = implode( ',', $font_name[$dir]['name'] );
            $font_name[$dir]['slug'] = sanitize_title($font_name[$dir]['name']);
        }

        return $font_name;
    }

    public function parseFontWeight( $fonts ) 
    {
        $font_weight = array();
        foreach ($fonts as $dir => $font) {
            $font_weight[$dir]['weight'] = array_unique($font);
            sort($font_weight[$dir]['weight']);
            $font_weight[$dir]['weight'] = implode( ',', $font_weight[$dir]['weight'] );
        }
        return $font_weight;
    }

    public function deleteDirectory() 
    {
        $response = array();
        $response['error'] = 1;
        $response['message'] = 'Cannot delete folder';

        $nonce = $_POST['nonce'];
        if ( ! wp_verify_nonce( $nonce, 'dev-options-nonce' ) )
            die ( 'Cannot verify nonce');

        $dirname = $_POST['folder_name'];

        if( $this->removeDirRecursive($this->font_dir. '/' .$dirname) ) {
            $option = get_option($this->option_name);

            if( $option && is_array($option) && in_array($dirname, $option) ) {
                $key = array_search($dirname, $option);
                unset($option[$key]);
            }

            update_option($this->option_name, $option);

            $response['error'] = 0;
            $response['message'] = 'Folder successfully deleted';
        }

        echo json_encode( $response );
        die();
    }

    public function upload() 
    {
        $nonce = $_POST['nonce'];
        if ( ! wp_verify_nonce( $nonce, 'dev-options-nonce' ) )
            die ( 'Cannot verify nonce');

        WP_Filesystem();
        $fileName = $_FILES['file']['name'];
        $folder_name = uniqid();

        if( move_uploaded_file($_FILES['file']['tmp_name'], $this->font_dir. '/' . $fileName) ) {
            wp_mkdir_p($this->font_dir. '/' .$folder_name);
            $unzip = unzip_file( $this->font_dir. '/' .$fileName, $this->font_dir. '/' .$folder_name);
            if ( $unzip ) {
                unlink( $this->font_dir. '/' .$fileName );

                $files = new \RecursiveDirectoryIterator($this->font_dir. '/'. $folder_name, \RecursiveDirectoryIterator::SKIP_DOTS);
                $iterator = new \RecursiveIteratorIterator($files);

                $has_font_file = false;

                foreach ($iterator as $fileinfo) {
                    if( in_array($fileinfo->getExtension(), $this->font_ext) ) {
                        $has_font_file = true;
                        break;
                    }
                }

                if ( glob($this->font_dir. '/' .$folder_name.'/*.css') && $has_font_file ) {
                    $this->response['error'] = 0;
                    $this->response['message'] = 'Files successfully extracted';
                    $this->response['meta'] = $this->getFontMeta( $folder_name );
                }else{
                    $this->response['error'] = 1;
                    $this->response['message'] = 'Files didn\'t contain stylesheet and font file';
                    $this->response['meta'] = $this->font_dir.$folder_name.'/*'.$font_format;
                    $this->removeDirRecursive($this->font_dir. '/' .$folder_name);
                }
            } else {
                $this->response['error'] = 1;
                $this->response['message'] = 'Some errors occured when extract the file';
            }
        }else{
            $this->response['error'] = 1;
            $this->response['message'] = 'File cannot uploaded';
        }

        echo json_encode( $this->response );

        die();
    }

    public function changeStatus()
    {
        $response = array();
        $response['error'] = 1;
        $response['message'] = 'Cannot change status';

        $nonce = $_POST['nonce'];
        if ( ! wp_verify_nonce( $nonce, 'dev-options-nonce' ) )
            die ( 'Cannot verify nonce');

        $dirname = $_POST['folder_name'];
        $status = $_POST['status'];

        $option = get_option($this->option_name);

        if( $option && is_array($option) ) {
            add_option($this->option_name, array());
        }

        switch ($status) {
            case 'add':
                if( !in_array($dirname, $option) ) {
                    $option[] = $dirname;
                }

                break;
            case 'remove':
                if( in_array($dirname, $option) ) {
                    $key = array_search($dirname, $option);
                    unset($option[$key]);
                }
                break;
        }

        update_option($this->option_name, $option);

        $response['error'] = 0;
        $response['message'] = 'Status changed';

        echo json_encode( $response );
        die();
    }

    public function getDirStatus($dirname) {
        $status = 0;
        $option = get_option($this->option_name);

        if( $option && in_array($dirname, $option) && is_dir($this->font_dir . '/' . $dirname) ) {
            $status = 1;
        }

        return $status;
    }

    public function removeDirRecursive($dirname)
    {
        $status = false;
        if( is_dir($dirname) ) {
            $dir = new \RecursiveDirectoryIterator($dirname, \RecursiveDirectoryIterator::SKIP_DOTS);
            foreach (new \RecursiveIteratorIterator($dir, \RecursiveIteratorIterator::CHILD_FIRST ) as $filename => $file) {
                if (is_file($filename))
                    unlink($filename);
                else
                    rmdir($filename);
            }

            if( rmdir($dirname) ) {
                $status = true;
            }
        }

        return $status;
    }

    public function enqueueStyles()
    {
        $option = get_option($this->option_name);

        if( $option && is_array($option) && !empty($option) ) {
            foreach ($option as $dir) {
                $css = glob($this->font_dir. '/' .$dir.'/*.css');

                if( !empty($css) ) {
                    wp_register_style($dir, $this->font_uri . '/' . $dir . '/' . basename($css[0]));
                    wp_enqueue_style($dir);
                }
            }
        }
    }
}