<?php
namespace Theme\Admin\DeveloperOptions;

use Theme\Admin\DeveloperOptions\inc\ModuleGenerator;
use Theme\Admin\DeveloperOptions\inc\WidgetGenerator;
use Theme\Admin\DeveloperOptions\inc\FontLoader;
use Timber\Timber;

class DeveloperOptions
{
    public function __construct()
    {
        add_action( 'admin_menu', [$this, 'createAdminPage'] );
        add_action( 'admin_enqueue_scripts', [$this, 'enqueueAssets'] );

        new ModuleGenerator();
        new WidgetGenerator();
        new FontLoader();
    }

    public function createAdminPage()
    {
        add_options_page(
            'Dev Options',
            'Dev Options',
            'manage_options',
            'dev_options',
            [$this, 'getPageCallable']
        );
    }

    public function getPageCallable()
    {
        $context = Timber::get_context();
        
        $font = new FontLoader();
        $context['fonts'] = $font->getFontMeta();
        $context['max_upload_size'] = size_format( wp_max_upload_size() );

        Timber::render('DeveloperOptions/template.twig', $context);
    }

    public function enqueueAssets($hook)
    {
        if ( 'settings_page_dev_options' != $hook ) {
            return;
        }

        wp_enqueue_style('dev-options', get_template_directory_uri() . '/src/Admin/DeveloperOptions/main.css');
        wp_enqueue_script('plupload-all');
        wp_enqueue_script('dev-options', get_template_directory_uri() . '/src/Admin/DeveloperOptions/main.js', null, false, true);
        wp_localize_script( 'dev-options', 'dev_options_object',
            array( 
                'ajaxurl'         => admin_url( 'admin-ajax.php' ),
                'nonce'           => wp_create_nonce('dev-options-nonce'),
                'max_upload_size' => size_format( wp_max_upload_size() )  
            )
        );
    }
}