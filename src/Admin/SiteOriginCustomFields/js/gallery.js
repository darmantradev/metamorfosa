(function($) {
    $(document).ready(function() {
      imagesList.button_events();
    });
  
    /*
    * Init drag and drop function after widget panel loaded
     */
    $( document ).on( 'sowsetupformfield', '.siteorigin-widget-field-type-gallery', function () {
      imagesList.init_sortable();
    });
  
    var selector = {
      media_button: '.imm_open-media-library',
      delete_image_button: '.imm_del-image',
      image_wrapper: '.imm_images-wrapper',
      single_image_wrapper: '.imm_image-block',
      clear_all_button: '.imm_clear-all',
      field_wrapper: '.siteorigin-widget-field',
      image_placeholder: 'imm_image-sortable-placeholder',
      main_field: '.imm_main-field'
    };
  
    var imagesList = {
      _frame: null,
  
      //Create media dialog frame
      create_media_frame: function( $el ) {
        imagesList._frame = wp.media.frames.file_frame = wp.media({
            title: 'Select Images',
            library: {
              type: 'image'
            },
            button: {
                text: 'Select Images'
            },
            multiple: 'add'
        });
  
        imagesList.media_events( $el );
      },
  
      //Reset frame
      reset_frame: function() {
        if( imagesList._frame ) {
          imagesList._frame.detach();
          imagesList._frame = null;
        }
      },
  
      //Get images selection and insert to DOM
      get_selection: function( $el ) {
        var attachment = imagesList._frame.state().get('selection').toJSON();
        var current_id = [];
        $el.find(selector.image_wrapper).html('<ul></ul>');
  
        if( attachment.length > 0 ) {
          $( selector.clear_all_button ).fadeIn().css("display","inline-block");
        }
  
        $.each(attachment, function(index, image) {
          var thumb = image.sizes.thumbnail.url;
          var id = image.id;
  
          var template = '<li class="'+ selector.single_image_wrapper.replace('.', '') +'">'+
                          '<a href="javascript:void(0)" class="'+ selector.delete_image_button.replace('.', '') +'"><span class="dashicons dashicons-no"></span></a>'+
                          '<img src="' + thumb + '" data-id="' + id + '" />'+
                          '<input type="hidden" class="siteorigin-widget-input" name="'+ $el.find(selector.media_button).data('field-name') + '[]" value="' + id + '">';
          $el.find(selector.image_wrapper).find('ul').append(template);
  
          //Reset input name on repeater
          if( $el.closest('.siteorigin-widget-field-repeater').length > 0 ) {
            var name = $el.closest('.siteorigin-widget-field-repeater').data('repeater-name');
            imagesList.reinit_repeater_setup( $el.closest('.siteorigin-widget-field-repeater-items'), name );
          }
          imagesList.init_sortable();
        });
      },
  
      //https://github.com/siteorigin/so-widgets-bundle/blob/develop/base/js/admin.js (Line:521)
      reinit_repeater_setup: function( $el, name ) {
        var $rptrItems = $el.find('> .siteorigin-widget-field-repeater-item');
        $rptrItems.each(function (i, el) {
          $(el).find('.siteorigin-widget-input').each(function (j, input) {
            var pos = $(input).data('repeater-positions');
            if (typeof pos === 'undefined') {
              pos = {};
            }
  
            pos[name] = i;
            $(input).data('repeater-positions', pos);
          });
        });
  
        // Update the field names for all the input items
        $el.find('.siteorigin-widget-input').each(function (i, input) {
          var $in = $(input);
          var pos = $in.data('repeater-positions');
  
          if (typeof pos !== 'undefined') {
            var newName = $in.attr('data-original-name');
  
            if (!newName) {
              $in.attr('data-original-name', $in.attr('name'));
              newName = $in.attr('name');
            }
            if (!newName) {
              return;
            }
  
            if (pos) {
              for (var k in pos) {
                newName = newName.replace('#' + k + '#', pos[k]);
              }
            }
            $in.attr('name', newName);
          }
        });
      },
  
      //Preselect images
      preselect_images: function( $el ) {
        var selection = imagesList._frame.state().get('selection');
        $.each( $el.find( selector.single_image_wrapper ), function(index, item) {
            var id = $(this).find('img').data('id');
            var attachment = wp.media.attachment(id);
            attachment.fetch();
            selection.add( attachment ? [ attachment ] : [] );
        });
      },
  
      //Delete single image
      delete_single_image: function( $el ) {
        var image_id =  $el.closest( selector.single_image_wrapper ).find('img').data('id');
  
        $el.closest( selector.single_image_wrapper ).fadeOut('fast', function() {
          $(this).remove();
        });
      },
  
      //Clear all images
      clear_all_images: function( $el ) {
        var warning = confirm( "This action cannot be undone. Do you want to continue?" );
  
        if( warning ) {
          $el.closest( selector.field_wrapper ).find( selector.image_wrapper ).html('');
          $el.hide();
        }
      },
  
      //Enable drag and drop
      init_sortable: function() {
        $(selector.image_wrapper).find('ul').sortable({
          placeholder: selector.image_placeholder
        });
  
        $(selector.image_wrapper).disableSelection();
      },
  
      //Wp.media events
      media_events: function( $el ) {
        imagesList._frame.on('open', function() {
          imagesList.preselect_images( $el );
        });
  
        imagesList._frame.on('select', function() {
          imagesList.get_selection( $el );
        });
      },
  
      //Button event
      button_events: function() {
        //Open media library
        $(document).on('click', selector.media_button, function(e) {
          e.preventDefault();
          imagesList.reset_frame();
          imagesList.create_media_frame( $(this).closest( selector.field_wrapper ) );
          imagesList._frame.open();
        });
  
        //Delete single image button
        $(document).on('click', selector.delete_image_button, function(e) {
          e.preventDefault();
          imagesList.delete_single_image( $(this) );
        });
  
        //Clear all images button
        $(document).on('click', selector.clear_all_button, function(e) {
          e.preventDefault();
          imagesList.clear_all_images( $(this) );
        });
      },
  
    };
  })( jQuery );
  