<?php
namespace Theme\Admin\SiteOriginCustomFields;

class SiteOriginCustomFields
{
    public function __construct()
    {
        add_filter( 'siteorigin_widgets_field_class_prefixes', [$this, 'setClassPrefix'] );
        add_filter( 'siteorigin_widgets_field_class_paths', [$this, 'setFieldPath'] );
    }

    public function setClassPrefix( $class_prefixes )
    {
        $class_prefixes[] = 'Theme_Custom_Field_';
        return $class_prefixes;
    }

    public function setFieldPath( $class_paths )
    {
        $class_paths[] = dirname(__FILE__) . '/class/';
        return $class_paths;
    }
}