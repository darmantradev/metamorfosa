<?php
class Theme_Custom_Field_Gallery extends SiteOrigin_Widget_Field_Base
{
    protected function render_field( $value, $instance )
    {
        $context = Timber::get_context();
        $context['value'] = $value;
        $context['name'] = esc_attr( $this->element_name );
        $context['id'] = esc_attr( $this->element_id );
        
        Timber::render(["SiteOriginCustomFields/template/gallery.twig"], $context);
    }

    protected function sanitize_field_input( $value, $instance ) {
        return $value;
    }

    public function enqueue_scripts() 
    {
        $path = get_template_directory_uri() . '/src/Admin/SiteOriginCustomFields';
        wp_enqueue_script( 'gallery-field', $path . '/js/gallery.js', array('jquery'), false, true );
        wp_enqueue_style( 'gallery-field', $path . '/css/gallery.css' );
    }
}