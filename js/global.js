const global = {
    init: () => {
        //sweet scroll
        document.addEventListener('DOMContentLoaded', () => {
            const sweetScroll = new SweetScroll({
                easing: 'easeInSine',
                offset: -200
            });
        }, false);
        
        //sal.js
        sal({
            threshold: 0.1,
            disabled: document.documentElement.classList.contains('touchevents') ? true : false
        });
    }
}

global.init();