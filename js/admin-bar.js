const adminBar = {
    init: () => {
        const adminBarToggle = document.getElementById('wpadminbar-toggle');

        if( ! document.body.contains( adminBarToggle ) ) {
            return false;
        }

        adminBarToggle.addEventListener('click', (e) => {
            e.preventDefault();
            e.currentTarget.classList.toggle('active');
            document.getElementById('wpadminbar').classList.toggle('active');
        });
    }
}

adminBar.init();