const socialMediaShare = {
    init: () => {
        const share_element = document.querySelectorAll('.social-media-share');

        if( !document.body.contains(share_element[0]) ) {
            return false;
        }

        share_element.forEach(element => {
            element.querySelectorAll('a').forEach(link => {
                link.addEventListener('click', e => {
                    if( e.target.getAttribute('data-popup') == 1 ) {
                        e.preventDefault();
                        const url = e.target.getAttribute('href');

                        if( e.target.getAttribute('data-title') == 'copy_link' ) {
                            let tempInput = document.createElement('input');
                            document.body.appendChild(tempInput);
                            tempInput.setAttribute('value', link);
                            tempInput.select();
                            document.execCommand('copy');
                            document.body.removeChild(tempInput);
                        }else {
                            const NWin = window.open(url, '', 'height=400,width=800');
                            if (window.focus) {
                                NWin.focus();
                            }
                        }
                    }
                });
            });
        });
    }
}

  socialMediaShare.init();